partially_applied_functions

 partially apply a function with an underscore, which gives you another function

def sumOfABC =( A:Int,B:Int,C:Int) => (A+B+C)

sumOfABC(10,20,30)

val part1 = sumOfABC(10,20,_:Int)
val part2plus30 = part1(30) =====> 60

val add1 = sumOfABC(10,_:Int,_:Int)
val add1plus20plus30 = add1(20,30)