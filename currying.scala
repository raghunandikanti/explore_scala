currying

def sumOfABC(A:Int)(B:Int)(C:Int) = (A+B+C)

//Regular
val part1 = sumOfABC(10)(20)(30)

//Currying
val part2 = sumOfABC(10)(20)(_)

part2(30)

def sumOfXYZ(X:Int,Y:Int,Z:Int) = (X+Y+Z)
sumOfXYZ(10,20,30)
======================================================================================================
Convert an existing function into Curried Function by using .curried()

val sumOfXYZCurried = (sumOfXYZ _).curried
val p1 = sumOfXYZCurried(10)(20)(30)       ===> Int=60
val p2 = sumOfXYZCurried(10)(20)(_)        ===> Int => Int = <function1>
val p3 = sumOfXYZCurried(10)(_)			   ===> Int => Int = <function1>
val p2add30 = p2(30)					   ===> Int = 30
val p3add10n20 = p3(10)(20)				   ===> Int = 40
======================================================================================================
convert a curried function into uncurried
scala> val sumOfXYZUnCurried = Function.uncurried(sumOfXYZCurried)
sumOfXYZUnCurried: (Int, Int, Int) => Int = <function3>

scala> sumOfXYZUnCurried(10,20,30)
res32: Int = 60

scala> sumOfXYZUnCurried(10,20)
<console>:17: error: not enough arguments for method apply: (v1: Int, v2: Int, v3: Int)Int in trait Function3.
Unspecified value parameter v3.
       sumOfXYZUnCurried(10,20)
                        ^
======================================================================================================