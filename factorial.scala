factorial

def factorial(n:Int):Int= {
 if (n ==0) 1
 else
   factorial(n-1) * n
 }

def factorials(x:Int):Int = {
    def go(x:Int, acc:Int):Int =
	if(x<=0) acc
	else go(n-1, n*acc)
go(n,1)
}