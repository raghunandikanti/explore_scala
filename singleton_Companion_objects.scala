Singleton Object
object Singles extends App{
def disp():Unit = println("From Object Singles")
}

Companion Object
class Companion {
def disp():Unit = println("From Class Companion")
}
object Companion extends App{
def disp():Unit = println("From Object Companion")
}