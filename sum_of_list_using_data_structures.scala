sealed trait List[+A]

case object Nil extends List[Nothing]
case object Cons[+A](head:A, tail:List[A]) extends List[A]

object List extends App {

def sum[ints:List[Int]):Int = ints match {
	case Nil => 0
	case Cons[h, t] = h + sum(t)
	}
	val total = sum(List(10,20,30))
}