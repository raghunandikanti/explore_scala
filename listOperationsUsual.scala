object listOperationsUsual extends App {

  val numbers = List[Int](1, 2, 30, 22, 13, 93, 3839)

  println("Given List  ")

  numbers.foreach(println)

  println("List of Odd numbers only")

  numbers.filter(x => (x % 2 == 1)).foreach(println)

  println("List of Even numbers only")

  numbers.filter(x => (x % 2 != 1)).foreach(println)

}