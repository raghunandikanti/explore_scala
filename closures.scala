closure - A function object that captures free variables, 
and is said to be "closed" over the variables visible at the time it is created.

val more = 10

val increase = (x:Int) => (x+more)

increase(100)