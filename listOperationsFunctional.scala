object listOperationsFunctional extends App {

  val numbers = List[Int](1, 2, 30, 22, 13, 93, 3839)

  println("Given List  ")

  for (x <- numbers)
    println

  println("List of Odd numbers only")

  for (x <- numbers) {
    if (x % 2 == 1) {
      print(x + " ")
    }
  }

  println("\n List of Even numbers only")

  for(x<- numbers)
    if(x%2 != 1)
      println(x + " ")
}