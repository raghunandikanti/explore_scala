match_case

Note:
object matchCase {

  def main(args: Array[String]) {
    val month = "DEC"
    month match {
      case "JAN" => println("January")
      case "FEB" => println("February")
      case "NOV" => println("November")
    }
  }
}

Exception in thread "main" scala.MatchError: DEC (of class java.lang.String)
	at com.premierinc.renaissance.spark.explore.matchCase$.main(matchCase.scala:11)
	at com.premierinc.renaissance.spark.explore.matchCase.main(matchCase.scala)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:497)
	at com.intellij.rt.execution.application.AppMain.main(AppMain.java:144)
	
To avoid the above Run time error please use the case default. 

object matchCase {

  def main(args: Array[String]) {
    val month = "DEC"
    month match {
      case "JAN" => println("January")
      case "FEB" => println("February")
      case "NOV" => println("November")
      case _ => println("Oops.. No matching case Statements !!!")
    }

  }