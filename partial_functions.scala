partial functions

val numToString = new PartialFunction[Int, String] {
val nums = Array("One", "Two" , "Three", "Four" , "Five")
def apply(i:Int) = nums(i-1)
def isDefinedAt(i: Int) = i>0 && i<6
}

numToString(3) ==> Three
============================================
val convert1To5 = new PartialFunction[Int, String]{
 val nums = Array("One", "Two", "Three", "Four", "Five")
 def apply(i:Int) = nums(i-1)
 def isDefinedAt(i:Int) = i>0 && i<6
}
val convert6To10 = new PartialFunction[Int, String]{
 val nums = Array("Six", "Seven", "Eight", "Nine", "Ten")
 def apply(i:Int) = nums(i-6)
 def isDefinedAt(i:Int) = i>5 && i<11
}
val handle1To10 = convert1To5 orElse convert6To10
handle1To10(7)
handle1To10(3)
============================================
val samples = (1 to 20).toList
val isEven:PartialFunction[Int,String] ={
case x if x%2 ==0 => x + "is Even"
}
val isOdd:PartialFunction[Int, String] ={
case x if x%2 ==1 => x + "is Odd"
}

val oddNumbers = samples.collect(isOdd)
val evenNumbers = samples.collect(isEven)
val numbers = samples.collect(isOdd orElse isEven)
=============================================