sum Of Numbers in Scala
=========================
val numbers = (1 to 10).toList

// Solution # 1
var sum = 0
for (x <- numbers)  sum+=x

// Solution # 2
var sum = 0
numbers.foreach{x=> (sum+=x)}

//Solution # 3
def sumOfTwo(x:Int, y: Int):Int = (x+y)

numbers.reduceLeft(sumOfTwo)
numbers.reduceRight(sumOfTwo)

//Solution # 4

numbers.sum

 
