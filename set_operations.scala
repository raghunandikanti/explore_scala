set operations
//Creating a set
val numbers1to10 = Set(1,2,3,4,5,6,7,8,9,10)
val oddNumbers = Set(1,3,5,7,9,11,13,15,17,19,21,23,25)
//Range to a Set
val numbers1to25 = (1 to 25).toSet

//Difference between Sets
val evenNumbers = numbers1to25.diff(oddNumbers)

//Intersection between Sets
val odds = numbers1to25.intersect(oddNumbers)

//check whether an element present or not

val is5there = numbers1to25.contains(5)   => true
val is500there = numbers1to25.contains(500)   => false
