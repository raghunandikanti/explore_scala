reduceRight() 
==============
// Sum Of Two Numbers
def sumOfTwo(x:Int, y: Int):Int = (x+y)

//Maximum Of Two Numbers
def maxOfTwo(x:Int, y: Int):Int = {
if (x > y) x else y
}

//Minimum Of Two Numbers
def minOfTwo(x:Int, y:Int):Int = {
if (x > y) y else x
}

sumOfTwo(10,15)
maxOfTwo(15,10)
minOfTwo(10,15)

val numbers = List[Int](1,2,3,4,5,6,7,8,9,10)
val sum = numbers.reduceRight(sumOfTwo)
val maximum = numbers.reduceRight(maxOfTwo)
val minimum = numbers.reduceRight(minOfTwo)